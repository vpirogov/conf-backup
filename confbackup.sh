#!/usr/bin/env bash

## PREPARE
SCRIPT=`realpath -m $0`
ROOT=`dirname $SCRIPT`
WORKDIR="$ROOT/backup"
cd $WORKDIR

SOURCES=`cat $WORKDIR/sources.txt`

function bind_textfile {
  if [ ! -f "$TARGET" ]; then
    mkdir -p `dirname $TARGET`
    touch $TARGET
  fi
  sudo mount --bind -oro $s $TARGET
}

function bind_directory {
  if [ ! -d "$TARGET" ]; then
    mkdir -p $TARGET
  fi
  sudo mount --bind -oro $s $TARGET
}

function mount_targets {
  for s in $SOURCES; do

    TYPE=`file -bL $s`
  	TARGET=`realpath -m $WORKDIR/$s`
    MOUNTED=`findmnt $TARGET`

    if [[ $MOUNTED != '' ]]; then
      sudo umount "$TARGET"86-SURGUT-AGG13-PEAGG-1
    fi

    case $TYPE in
  		'directory')
        bind_directory
        echo -e "directory \e[0;32m$s\e[0m mounted as '\e[32m$TARGET\e[0m'"
  			;;
  		*'ASCII text'*)
  		  bind_textfile
        echo -e "text file '\e[32m$s\e[0m' mounted as '\e[32m$TARGET\e[0m'"
  			;;
  		*)
  			echo -e "ignoring '\e[31m$s\e[0m' because it is a '\e[0;33m$TYPE\e[0m'"
  	esac
  done
}

function umount_targets {
  for s in $SOURCES; do
  	TARGET=`realpath -m $WORKDIR/$s`
    MOUNTED=`findmnt $TARGET`
    if [[ $MOUNTED != '' ]]; then
      sudo umount $TARGET
      echo -e "$TARGET unmounted"
    fi
  done
}

function status {
  for s in $SOURCES; do
  	TARGET=`realpath -m $WORKDIR/$s`
    MOUNTED=`findmnt $TARGET`
    if [[ $MOUNTED != '' ]]; then
      echo -e "$TARGET \e[32mmounted\e[0m"
    else
      echo -e "$TARGET \e[33mnot mounted\e[0m"
    fi
  done
}

function run_git_tasks {
  STATUS=`git status 2>&1`
  if [[ $STATUS =~ "Not a git repo" ]]; then
    git init
    STATUS=`git status 2>&1`
  fi
  if [[ $STATUS =~ "Untracked files:" ]]; then
    git add .
    git commit
    git push
    STATUS=`git status 2>&1`
  fi
  if [[ $STATUS =~ "Changes not staged for commit:" ]]; then
    git add .
    git commit
    git push
    STATUS=`git status 2>&1`
  fi
  REMOTE=`git remote -v`
  if [[ ! $REMOTE ]]; then
    echo -e "git remote is \e[31mnot configured\e[0m"
    echo -e "cd $WORKDIR"
    echo -e "git remote add origin <your git repo>"
    echo -e "git push --set-upstream origin master"
    exit 1
  fi
  echo $STATUS
}

case $1 in
  'git')
    run_git_tasks
    exit 0
    ;;
  'mount')
    mount_targets
    sleep 1
    ;;
  'umount')
    umount_targets
    ;;
  'status')
    echo "sources:" $SOURCES
    status
    ;;
  'save')
    $SCRIPT 'mount'
    $SCRIPT 'git'
    $SCRIPT 'umount'
    ;;
  *)
    status
esac
